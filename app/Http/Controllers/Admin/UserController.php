<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\File;
use App\Models\UserFile;


use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('created_at','desc')->get();

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        

        return view('admin.users.create');
    }


    public function store(Request $request)
    {
            $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users'
        ]);

        $password = $this->random_password(8);
        $users = $request->all();
        $users['password'] = bcrypt($password);
        $users['user_password'] = $password;
        $user = User::create($users); 

        return \Redirect::Route('admin.users.index');   
    }

    public function edit($id)
    {
        $user = User::find($id);
        $files = File::get();
        $userfiles = UserFile::where('user_id',$id)->get();

        return view('admin.users.edit', compact('user','files','userfiles'));
    }


    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'name' => 'required|max:255', 
        ]);

        $users = $request->all(); 
        $user = User::find($id)->update($users); 
        UserFile::where('user_id',$id)->delete();
        foreach($request->filesaccess as $file)
        {
            UserFile::create(['user_id'=>$id,'file_id'=>$file]);
        }
        return \Redirect::Route('admin.users.index');   

    }


        
        function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
        }
}
