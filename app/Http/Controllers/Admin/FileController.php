<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\File;


use Illuminate\Http\Request;

class FileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = File::orderBy('created_at','desc')->get();

        return view('admin.files.index', compact('files'));
    }

    public function create()
    {
        

        return view('admin.files.create');
    }


    public function store(Request $request)
    {
            $this->validate($request, [
            'name' => 'required|max:255', 
        ]);

       
        $files = $request->all(); 
        
        $fileName = time().'.' . $request->file('file')->getClientOriginalExtension(); 
        $path = storage_path('app/public/files/');
        $music_file = $request->file('file'); 
        $music_file->move($path,$fileName); 
        $files['file'] = $fileName;


        File::create($files); 

        return \Redirect::Route('admin.files.index');   
    }


        
        function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
        }
}
