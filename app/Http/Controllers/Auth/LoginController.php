<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user';    
          

    public function login(Request $request)
    {
        $auth = false;
        $credentials = $request->only('email','password');

        // if (\Auth::guard('user')->attempt($credentials, $request->has('remember'))) {
        if (\Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
        $auth = true; // Success
        }

        if($auth != true){
        $msg = "Credentials are incorrect";
        }else{
          return \Redirect::Route('user.home'); 
        }

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
