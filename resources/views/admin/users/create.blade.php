

@extends('admin.layout.base')

@section('content')

<!--DataTables example Row grouping-->
              <div id="row-grouping" class="section">
               
                <div class="row">

                  
                  <div class="col s12">
                    
                       {!! Form::open(array('route' => 'admin.users.store', 'method' => 'post','files'=>true)) !!}
          
						
				<div class="form-group s12"> 
					<div class="col-xs-10">
					 <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                        @if ($errors->has('name'))
                          <span class="help-block">{{ $errors->first('name') }}</span>
                        @endif 
                      </div>
					</div>
                </div>
                
				<div class="form-group s12"> 
					<div class="col-xs-10">
					 <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::text('email', null, ['class'=>'form-control']) !!}
                        @if ($errors->has('email'))
                          <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif 
                      </div>
					</div>
				</div>

			
				 
				<div class="form-group s12">
					<label for="zipcode" class="col-xs-12 col-form-label"></label>
					<div class="col-xs-10">
						 <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save </button>
					</div>
				</div>
				
			{!! Form::close() !!}
               </div>
                  </div>
                </div>
              </div>
            </div>


@endsection




