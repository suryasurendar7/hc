

@extends('admin.layout.base')

@section('content')

<!--DataTables example Row grouping-->
              <div id="row-grouping" class="section">
               
                <div class="row">

                  
                  <div class="col s12">
                    

                <a href="{{route('admin.users.create')}}" style="float:right;">Add Client</a>

                    <div id="data-table-row-grouping_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="data-table-row-grouping_length"><label>Show <div class="select-wrapper initialized"><span class="caret">▼</span><input type="text" class="select-dropdown" readonly="true" data-activates="select-options-6ad77ea2-0ccf-1740-837e-adb846dbf2f6" value="25"><ul id="select-options-6ad77ea2-0ccf-1740-837e-adb846dbf2f6" class="dropdown-content select-dropdown "><li class=""><span>10</span></li><li class=""><span>25</span></li><li class=""><span>50</span></li><li class=""><span>100</span></li></ul><select name="data-table-row-grouping_length" aria-controls="data-table-row-grouping" class="initialized" data-select-id="6ad77ea2-0ccf-1740-837e-adb846dbf2f6"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select></div> entries</label></div><div id="data-table-row-grouping_filter" class="dataTables_filter"></div><table id="data-table-row-grouping" class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="data-table-row-grouping_info" style="width: 100%;">
                      <thead>
                        <tr role="row"><th class="sorting" tabindex="0" aria-controls="data-table-row-grouping" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 218px;">Name</th><th class="sorting" tabindex="0" aria-controls="data-table-row-grouping" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 218px;">Email</th></tr>
                      </thead>
                      <tfoot>
                         @foreach($users as $user)
                        <tr><td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                          <td><a href="{{ route('admin.users.edit',$user->id)}}">Edit</a></td>
                    </tr>
                        @endforeach

                      </tfoot>
                      <tbody>
                        
                       
                        
                      </tbody>
                    </table></div>
                  </div>
                </div>
              </div>
            </div>


@endsection