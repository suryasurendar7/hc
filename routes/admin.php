<?php

    /* Route::get('/', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.home');
})->name('home'); */

Route::get('/', 'Admin\AdminController@index')->name('home');

Route::resource('users', 'Admin\AdminController');
Route::resource('files', 'Admin\AdminController');